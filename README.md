# Machine overview

I wanted to quickly make a small machine I could learn from, practice using the Zünd and EDM, and take to Fab14, so I designed what became Ischeau. Limiting myself to a carry-on the body of the machine is 12"x12"x12". I started cutting metal a week before Fab14, but finished the body in time to back it up and ship to France. All of the electroncis I ended up doing at the conference, which was really straight forward because I was using a SmoothieBoard, so all I had to do was wire up the motors, thermistors etc. plug them in, and check the config. file.

[SmoothieBoard pinout](http://smoothieware.org/pinout)

# CAD

This is an H-bot machine, with 4 linear rod rails, 6 linear bearings, a linear rail and carriage, and a lead screw for the Z. Here's a rendering of the final CAD ready for fabrication.

![cad render](/images/cad_render_02.JPG)

## Top view
![cad render](/images/cad_render_03.JPG)

## Side view
![cad render](/images/cad_render_04.JPG)

# Bill of Materials

|*Item*             |*Vender*  |
|-------------------|----------|
|Heated Bed         | [Amazon - Ultrabase](https://www.amazon.com/220x220x5-5mm-Ultrabase-Platform-3D-Printer/dp/B07GS2F921/ref=sr_1_9?ie=UTF8&qid=1536005068&sr=8-9&keywords=ultrabase+220x220)   |
|Extruder           | [E3D](https://e3d-online.com/)|
|SmoothieBoard      | [SmoothieWare](http://smoothieware.org/smoothieboard)|
|USB B extender     | [Amazon](https://www.amazon.com/StarTech-1-Feet-Panel-Mount-Cable/dp/B002M8VBIS/ref=sr_1_3?s=electronics&ie=UTF8&qid=1532392243&sr=1-3&keywords=usb+B+panel+mount) |
|24V 16A Supply     | found  |
|Power switch       | Amazon |
|2 NEMA 17          | found  |
|NEMA 17 with lead screw | found |
|Linear rail        | found, but [looks like...](https://www.amazon.com/Sliding-Guideway-Bearing-Precision-Measurement/dp/B06XFZQWCM/ref=sr_1_5?s=industrial&ie=UTF8&qid=1532391839&sr=1-5&keywords=linear+bearing)|
|Linear bearings    | [Amazon](https://www.amazon.com/PerfecTech-Linear-Bearing-Bushing-Printer/dp/B01LXX22B0/ref=sr_1_6?s=industrial&ie=UTF8&qid=1532391839&sr=1-6&keywords=linear+bearing) |
|8mm precision rod  | McMaster or Amazon |
|GT2 belt and pullys| [Amazon](https://www.amazon.com/Timing-Pulley-Printer-Beauty-Star/dp/B0776KXY8G/ref=sr_1_1_sspa?s=industrial&ie=UTF8&qid=1532391414&sr=1-1-spons&keywords=gt6+timing+belt&psc=1)|
|Toothless pulleys  | [Amazon](https://www.amazon.com/dp/B07BPHPYTN/?coliid=I2DW8P5EIRAYGJ&colid=YACM7I4IG922&psc=0&ref_=lv_ov_lig_dp_it)|
|Fnacy bed levelers | [Amazon](https://www.amazon.com/WINSINN-Leveling-Spring-Component-Platform/dp/B0761TLRNY/ref=sr_1_4?ie=UTF8&qid=1532391690&sr=8-4&keywords=3d+printer+bed)|
|Lots of Aluminium  | found  |
|Lots of M3         | in lab |
|2 M5               | in lab |

# Fabrication

A motivation in the design for this machine was a desire to practice routing on the Zünd. After punturing through holes with the waterjet I put the parts on the Zünd and used the optical registration feature to allign them. I made several of the 1/4" Al parts on th Zünd. Below is an image showing the edge of parts cut on the Zünd (on bottom) and waterjet (middle) and the surface of an Al sheet (top).

<img src="/images/IMG-3157.JPG" width=50%/>

And this is an angled view of that piece which was routed with a 6mm end mill, then finished with a 2mm to get smaller internal radiuses. It was tapped afterwards.

<img src="/images/IMG-3156.JPG" width=50%/>

The optical registration didn't proove accurate enough to re-allign parts after they'd been moved. I tried a few times and the second pass was visibly off. For rapidity of fabrication I ended up waterjetting about half of the peices, but the machine came together within the week.

<img src="/images/IMG-3087.JPG" width=50%/>

# Action

I made this 3 ply cardboard box to ship it to Fab14 and was very happy with it's preformance. Carrying the machine around I dropped it a few times but all was well.

<img src="/images/IMG-3089.JPG" width=50%/>

At Fab14 I wired it up. Configured the SmoothieBoard and started printing using Pronterface and Prusa slicer.

<img src="/images/DSC04920.JPG" width=50%/>

Ischeau can be accessed over the local network by this link:
http://192.168.1.105/sd/webif/new-web-interface/index.html

<img src="/images/in_action.mp4" width=50%/>

# Test prints

<img src="/images/IMG-3187.JPG" width=50%/>

<img src="/images/IMG-3188.JPG" width=50%/>

# Power budget

Ultrabase heated bed - 140W
Aero extruder - 40W
Which shouldn't be over the PSU rating of 16, but I'm seeing it dip...


# Things to impove next time

- pulley and belt allignment
- be able to see nozzle and bed touching from side
- add limit switche
- fix belt tensioner - FIXED
- power supply fan is blocked by table so heats up
- add fan for motorcontrollers